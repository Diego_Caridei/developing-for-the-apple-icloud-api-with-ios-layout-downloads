//
//  ViewController.swift
//  CloudKitExample
//
//  Created by Diego Caridei on 11/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit
import CloudKit

class ViewController: UIViewController {

    @IBOutlet weak var textDelete: UITextField!
    @IBOutlet weak var textForAdding: UITextField!
    @IBOutlet weak var textView: UITextView!
    
    //This is a reference of our db
    var db:CKDatabase!
    var currentRecords: [CKRecord] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        
        //Init
        db = CKContainer.defaultContainer().privateCloudDatabase
        self.loadCloudData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addAction(sender: AnyObject) {
        if textForAdding.text == "" || textForAdding.text == nil {
            return
        }else{
            //I  create a record (recordType is the name of the table)
            let itemRecord : CKRecord = CKRecord (recordType: "MyList")
            // setting the item (it is work like a dictionary key/value)
            itemRecord.setObject(textForAdding.text, forKey: "item")
            //Saving in the db
            db.saveRecord(itemRecord, completionHandler: { (record:CKRecord?, error:NSError?) in
                if error  == nil{
                    print("Record saved Successfully")
                    NSOperationQueue.mainQueue().addOperationWithBlock({ 
                        self.textForAdding.text = ""
                    })
                }
            })
        }
    }
    @IBAction func deleteAction(sender: AnyObject) {
        deleteRecord(textDelete.text!)
    }

    func  deleteRecord(recordName:String)  {
        var recordToDelete: CKRecord?
        for record in currentRecords {
            let item :String = record.objectForKey("item") as! String
            if item == recordName {
                recordToDelete = record
            }
        }
        if recordToDelete == nil {
            return
        }
       
        db.deleteRecordWithID(recordToDelete!.recordID) { (rID:CKRecordID?, error:NSError?) in
            if error != nil {
                return
            }
            NSOperationQueue.mainQueue().addOperationWithBlock({ 
                self.textView.text = "(Record deleted successfully)"
                self.textDelete.text = ""
                 self.loadCloudData()
            })
        }
        
    }
    
    func loadCloudData()  {
        let predicate : NSPredicate = NSPredicate(value: true)
        textView.text = "(Fetching cloud data ...)"
        let query = CKQuery(recordType: "MyList", predicate: predicate)
        db.performQuery(query, inZoneWithID: nil) { (records:[CKRecord]?, error:NSError?) in
            if error != nil || records == nil {
                return
            }
            self.currentRecords.removeAll()
            self.currentRecords = records!
            var aList:[String] = [""]
            for var i:Int = 0; i < records?.count; i += 1 {
                let record : CKRecord = records![i]
                aList.append(record.objectForKey("item") as! String)
            }
            NSOperationQueue.mainQueue().addOperationWithBlock({ 
                self.textView.text = aList.joinWithSeparator("\n")
            })
        }
    }
    
    @IBAction func RefreshAction(sender: AnyObject) {
        self.loadCloudData()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}

