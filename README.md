# Developing for the Apple iCloud API with iOS Layout Downloads Add to Playlist Share
iCloud is built into every iOS device and every new Mac. It lets users access their content from any device, but it's also great for developers who want to store and sync data in the cloud. In this short course, Todd Perkins explains how to access iCloud from iOS, using the iCloud API to incorporate storage capabilities into the iPhone and iPad apps you build. He shows how to load files from iCloud, send data to iCloud, work with CloudKit databases, and autosync cloud data so your apps are always up to date.
Topics include:
Working with iCloud key-value pairs
Building an example app that accesses iCloud documents
Saving and loading iCloud documents
Fetching records with CloudKit
