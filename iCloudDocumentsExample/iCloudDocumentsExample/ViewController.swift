//
//  ViewController.swift
//  iCloudDocumentsExample
//
//  Created by Diego Caridei on 10/05/16.
//  Copyright © 2016 Diego Caridei. All rights reserved.
//

import UIKit
var mainViewController: ViewController!

class ViewController: UIViewController, UIDocumentPickerDelegate {

    @IBOutlet weak var textView: UITextView!
    var document : CloudDoc!
    
    func getURL () -> NSURL {
        let cloudURL : NSURL  = NSFileManager.defaultManager().URLForUbiquityContainerIdentifier(nil)!
        // se voglio semplicemente salvare un document
        //let fileURL : NSURL = cloudURL.URLByAppendingPathComponent("doc.txt")
        
        //se vogliamo salvare all'interno icloud drive basta modificare il path Documents/doc.txt
        let fileURL : NSURL =  cloudURL.URLByAppendingPathComponent("Documents/doc.txt")
        return fileURL
    }
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewController  = self
        document = CloudDoc(fileURL: getURL())
        //verify if the file is correctly open
        document.openWithCompletionHandler { (success:Bool) in
            if !success{
                print("Error opening doc")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapSave(sender: AnyObject) {
        //Uploaded if the document is change
        document.updateChangeCount(.Done)
    }

    @IBAction func didTapLoad(sender: AnyObject) {
        let docPicker : UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.content"], inMode: UIDocumentPickerMode.Open)
        docPicker.delegate = self
        docPicker.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        self.presentViewController(docPicker, animated: true) { 
            //Handle Completion
        }
    }
    
    func documentPicker(controller: UIDocumentPickerViewController, didPickDocumentAtURL url: NSURL) {
        let data: NSData = NSData (contentsOfURL: url)!
        if data.length <= 0 {
            print("No data found")
            return
        }
        textView.text = String (data: data, encoding: NSUTF8StringEncoding)
    }
    
}

